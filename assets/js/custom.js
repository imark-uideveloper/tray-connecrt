(function($) {
  'use strict';
  
  $(document).ready(function(){
    
    // Wow Js
    var wow = new WOW({
      
        mobile: false // default
    })
    wow.init();
    
    
    //Input Field
    var formFields = jQuery('.form-group');

    formFields.each(function() {
        var field = jQuery(this);
        var input = field.find('.form-control');
        var label = field.find('label');

        function checkInput() {
            var valueLength = input.val().length;
            if (valueLength > 0 ) {
                label.addClass('freeze')
            } 
            else {
                label.removeClass('freeze')
            }
        }
        input.change(function() {
            checkInput()
        })
    });
    
    //Header

    $(window).on("load resize scroll", function(e) {
      var Win = $(window).height();
      var Header = $("header").height();
      var Footer = $("footer").height();

      var NHF = Header + Footer;

      $('.main').css('min-height', (Win - NHF));

    });
    

    //Textarea 

    $('.firstCap, textarea').on('keypress', function(event) {
        var jQuerythis = $(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase(),
        con = thisVal.slice(1, thisVal.length);
        $(this).val(FLC + con);
    });

  });

  
  //Page Zoom
  document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
      event.preventDefault();
    }
  }, false);

  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
})(jQuery)